

<div id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style="background-image:url('assets/img/1.png');"></div>
                <div class="carousel-caption slide-up">
                    <h1 class="banner_heading">ABC <span>President </span>Indonesia</h1>
                    <p class="banner_txt">PT ABC President Indonesia berdiri pada bulan September 1991 sebagai bentuk kerjasama antara PT ABC Central Food dari Indonesia dan Uni-President Enterprises Corporation dari Taiwan.</p>
                    <div class="slider_btn">
                        <button type="button" class="btn btn-default slide">Learn More <i class="fa fa-caret-right"></i></button>
                        <button type="button" class="btn btn-primary slide">Learn More <i class="fa fa-caret-right"></i></button>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="fill" style="background-image:url('assets/img/2.png');"></div>
                <div class="carousel-caption slide-up">
                    <h1 class="banner_heading">Produk <span>ABC </span>President</h1>
                    <p class="banner_txt">bidang usahanya telah merambah ke berbagai produk makanan jadi, minyak goreng, minuman, produk-produk berbahan susu, makanan sehat, makanan beku, dan sebagainya. Selain dalam bidang produksi makanan dan minuman, Uni-President Enterprises Corporation juga terjun dalam bidang ritel dengan memiliki beberapa jaringan toko, pasar swalayan dan convenience store.</p>
                    <div class="slider_btn">
                        <button type="button" class="btn btn-default slide">Learn More <i class="fa fa-caret-right"></i></button>
                        <button type="button" class="btn btn-primary slide">Learn More <i class="fa fa-caret-right"></i></button>
                    </div>
                </div>
            </div>

            <div class="item">
                <div class="fill" style="background-image:url('assets/img/3.png');"></div>
                <div class="carousel-caption slide-up">
                     <h1 class="banner_heading">ABC <span>President </span>Indonesia</h1>
                    <p class="banner_txt">PT ABC President Indonesia berdiri pada bulan September 1991 sebagai bentuk kerjasama antara PT ABC Central Food dari Indonesia dan Uni-President Enterprises Corporation dari Taiwan.</p>
                        <button type="button" class="btn btn-default slide">Learn More <i class="fa fa-caret-right"></i></button>
                        <button type="button" class="btn btn-primary slide">Learn More <i class="fa fa-caret-right"></i></button>
                    </div>
                </div>
            </div>
    

        <!-- Left and right controls -->

        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"> <i class="fa fa-angle-left" aria-hidden="true"></i>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"> <i class="fa fa-angle-right" aria-hidden="true"></i>
            <span class="sr-only">Next</span>
        </a>
    </div>



    <section id="process">
        <div class="container">
            <div class="section-heading text-center">
                <div class="col-md-12 col-xs-12">
                    <h1>What <span>We Product</span></h1>
                    <p class="subheading">bidang usahanya telah merambah ke berbagai produk makanan jadi, minyak goreng, minuman, produk-produk berbahan susu, makanan sehat, makanan beku, dan sebagainya. Selain dalam bidang produksi makanan dan minuman, Uni-President Enterprises Corporation juga terjun dalam bidang ritel dengan memiliki beberapa jaringan toko, pasar swalayan dan convenience store.</p>
                </div>
            </div>

            <div class="col-md-4 ">
                <div class="gambar">
                  <img src="assets/img/makanan1.png" alt="Avatar" class="image">
                  <div class="overlay">
                    <button type="button" class="btn" ><a href="<?=base_url().'produk/list_minuman'?>">Makanan</a></button>
                  </div>
                </div>
                
                <div class="process-text-block">
                        <h3><a href="#">Makanan</a></h3>
                        <p>Beberapa jenis prodak makanan di produksi oleh pt </p>
                    </div>
                </div>


                <div class="col-md-4 ">
                <div class="gambar">
                  <img src="assets/img/minuman.png" alt="Avatar" class="image">
                  <div class="overlay">
                    <button type="button" class="btn" ><a href="<?=base_url().'produk/list_minuman'?>">Minuman</a></button>
                  </div>
                </div>
                
                <div class="process-text-block ">
                        <h3><a href="#">Minuman</a></h3>
                        <p>Lorem ipsum dolor sit amet sit legimus copiosae instructior ei ut vix denique fierentis ea saperet inimicu ut qui dolor oratio mnesarchum</p>
                    </div>
                </div>

                <div class="col-md-4 ">
                <div class="gambar">
                  <img src="assets/img/export.png" alt="Avatar" class="image">
                  <div class="overlay">
                    <button type="button" class="btn" ><a href="<?=base_url().'produk/list_minuman'?>">Export and Private label</a></button>
                  </div>
                </div>
                
                <div class="process-text-block">
                        <h3><a href="#">Export and Private Label</a></h3>
                        <p>Lorem ipsum dolor sit amet sit legimus copiosae instructior ei ut vix denique fierentis ea saperet inimicu ut qui dolor oratio mnesarchum</p>
                    </div>
                </div>

            </div>

        </div>
    </section>


    <section id="about">
        <div class="image-holder col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-left">
            <div class="background-imgholder">
                <img src="assets/img/1.jpg" alt="about" class="img-responsive" style="display:none;" />
            </div>
        </div>

        <div class="container-fluid">

            <div class="col-md-7 col-md-offset-5 col-sm-8 col-sm-offset-2 col-xs-12 text-inner ">
                <div class="text-block">
                    <div class="section-heading">
                        <h1>ABOUT <span>US</span></h1>
                        <p class="subheading">Lorem ipsum dolor sit amet sit legimus copiosae instructior ei ut.</p>
                    </div>

                    <ul class="aboutul">
                        <li> <i class="fa fa-check"></i>Vix denique fierentis ea saperet inimicu ut qui dolor oratio mnesarchum.</li>
                        <li> <i class="fa fa-check"></i>legimus copiosae instructior ei ut vix denique fierentis atqui mucius consequat ad pro.</li>
                        <li> <i class="fa fa-check"></i>Ea saperet inimicu ut qui dolor oratio maiestatis ubique mnesarchum.</li>
                        <li> <i class="fa fa-check"></i>Sanctus voluptatibus et per illum noluisse facilisis quo atqui mucius ad pro.</li>
                        <li> <i class="fa fa-check"></i>At illum noluisse facilisis quo te dictas epicurei suavitate qui his ad.</li>
                        <li> <i class="fa fa-check"></i>Tantas propriae mediocritatem id vix qui everti efficiantur an ocurreret consetetur.</li>
                    </ul>

                    <button type="button" class="btn btn-primary slide">Learn More  <i class="fa fa-caret-right"></i> </button>


                </div>
            </div>
        </div>
    </section>


   

 <div id="fh5co-blog" class="animate-box">
        <div class="container">
            <div class="section-heading text-center">
                <div class="col-md-12 col-xs-12">
                    <h1>Artikel  <span>Terkait</span></h1>
                    <p class="subheading">Lorem ipsum dolor sit amet sit legimus copiosae instructior ei ut vix denique fierentis ea saperet inimicu ut qui dolor oratio mnesarchum ea utamur impetus fuisset nam nostrud euismod volumus ne mei.</p>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
            <?php
                foreach ($post->result_array() as $j) :
                    $post_id=$j['tulisan_id'];
                    $post_judul=$j['tulisan_judul'];
                    $post_isi=$j['tulisan_isi'];
                    $post_author=$j['tulisan_author'];
                    $post_image=$j['tulisan_gambar'];
                    $post_tglpost=$j['tanggal'];
                    $post_slug=$j['tulisan_slug'];
            ?>

            <div class="col-md-4">
                    <a class="fh5co-entry" href="<?php echo base_url().'artikel/'.$post_slug;?>">
                        <figure>
                        <img src="<?php echo base_url().'assets/images/'.$post_image;?>" alt="" class="img-responsive">
                        </figure>
                        <div class="fh5co-copy">
                            <h3><?php echo $post_judul;?></h3>
                            <span class="fh5co-date"><?php echo $post_tglpost.' | '.$post_author;?></span>
                            <?php echo limit_words($post_isi,20).'...';?>
                        </div>
                    </a>
                </div>
                <?php endforeach;?>

                <div class="slider_btn">
                        <button type="button" class="btn btn-primary slide">Learn More <i class="fa fa-caret-right"></i></button>
                    </div>
            </div>
        </div>
    </div>


    <section id="testimonial">
        <div class="container">
            <div class="section-heading text-center">
                <div class="col-md-12 col-xs-12">
                    <h1>Pemilik <span>Perusahaan</span></h1>
                    <p class="subheading">Lorem ipsum dolor sit amet sit legimus copiosae instructior ei ut vix denique fierentis ea saperet inimicu ut qui dolor oratio mnesarchum ea utamur impetus fuisset nam nostrud euismod volumus ne mei.</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4 col-sm-12 block ">
                    <div class="testimonial_box">
                        <p>Lorem ipsum dolor sit amet sit legimus copiosae instructior ei ut vix denique fierentis ea saperet inimicu ut qui dolor oratio mnesarchum ea utamur impetus fuisset. </p>
                    </div>
                    <div class="arrow-down"></div>
                    <div class="testimonial_user">
                        <div class="user-image"><img src="assets/img/user1.png" alt="user" class="img-responsive" /></div>
                        <div class="user-info">
                            <h5>Lorem Ipsum</h5>
                            <p>Manager</p>
                        </div>
                    </div>
                </div>


                <div class="col-md-4 col-sm-12 block">
                    <div class="testimonial_box">
                        <p>Lorem ipsum dolor sit amet sit legimus copiosae instructior ei ut vix denique fierentis ea saperet inimicu ut qui dolor oratio mnesarchum ea utamur impetus fuisset. </p>
                    </div>
                    <div class="arrow-down"></div>
                    <div class="testimonial_user">
                        <div class="user-image"><img src="assets/img/user1.png" alt="user" class="img-responsive" /></div>
                        <div class="user-info">
                            <h5>Lorem Ipsum</h5>
                            <p>Manager</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 block">
                    <div class="testimonial_box">
                        <p>Lorem ipsum dolor sit amet sit legimus copiosae instructior ei ut vix denique fierentis ea saperet inimicu ut qui dolor oratio mnesarchum ea utamur impetus fuisset. </p>
                    </div>
                    <div class="arrow-down"></div>
                    <div class="testimonial_user">
                        <div class="user-image"><img src="assets/img/user1.png" alt="user" class="img-responsive" /></div>
                        <div class="user-info">
                            <h5>Lorem Ipsum</h5>
                            <p>Manager</p>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </section>

    <div id="panel">
        <div id="panel-admin">
            <div class="panel-admin-box">
                <div id="tootlbar_colors">
                    <button class="color" style="background-color:#1abac8;" onclick="mytheme(0)"></button>
                    <button class="color" style="background-color:#ff8a00;" onclick="mytheme(1)"> </button>
                    <button class="color" style="background-color:#b4de50;" onclick="mytheme(2)"> </button>
                    <button class="color" style="background-color:#e54e53;" onclick="mytheme(3)"> </button>
                    <button class="color" style="background-color:#1abc9c;" onclick="mytheme(4)"> </button>
                    <button class="color" style="background-color:#159eee;" onclick="mytheme(5)"> </button>
                </div>
            </div>

        </div>
        <a class="open" href="#"><span><i class="fa fa-gear fa-spin"></i></span></a>
    </div>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url('assets/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo base_url('assets/jquery-easing/jquery.easing.min.js') ?>"></script>
<!-- Page level plugin JavaScript-->
<script src="<?php echo base_url('assets/chart.js/Chart.min.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/datatables/dataTables.bootstrap4.js') ?>"></script>
<!-- Custom scripts for all pages-->
<script src="<?php echo base_url('js/sb-admin.min.js') ?>"></script>
<!-- Demo scripts for this page-->
<script src="<?php echo base_url('js/demo/datatables-demo.js') ?>"></script>
<script src="<?php echo base_url('js/demo/chart-area-demo.js') ?>"></script>



</html>
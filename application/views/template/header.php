<!DOCTYPE html>
<html>
<style>
.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #3e8e41;
}
</style>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/scss/main.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/scss/skin.css">
    <link rel="stylesheet" href="<?= base_url()?>assets/scss/a.css">

    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url('assets/fontawesome-free/css/all.min.css') ?>" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="<?php echo base_url('assets/datatables/dataTables.bootstrap4.css') ?>" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url('css/sb-admin.css') ?>" rel="stylesheet">

    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="<?= base_url()?>assets/script/index.js"></script>
    <link rel="stylesheet" href="<?php echo base_url().'theme/css/bootstrap.css'?>">



    <script src="<?php echo base_url().'theme/js/modernizr-2.6.2.min.js'?>"></script>
    <?php
            error_reporting(0);
            function limit_words($string, $word_limit){
                $words = explode(" ",$string);
                return implode(" ",array_splice($words,0,$word_limit));
            }

        ?>
</head>

<body id="wrapper">

    <section id="top-header">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-7 col-xs-7 top-header-links">
                    <ul class="contact_links">
                        <li><i class="fa fa-phone"></i><a href="#">+62 333 333 333</a></li>
                        <li><i class="fa fa-envelope"></i><a href="#">abcpresident.com</a></li>
                    </ul>
                </div>
                <div class="col-md-5 col-sm-5 col-xs-5 social">
                    <ul class="social_links">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-skype"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        </div>

    </section>

    <header>
        <nav class="navbar navbar-inverse">
            <div class="container">
                 <div class="row">
                    <div class="navbar-header">

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
                        <a class="navbar-brand" href="<?php echo base_url().''?>"><img src="<?php echo base_url().'assets/images/abc.png'?>" width= "120 px"></a>


                    </div>
                    <div id="navbar" class="collapse navbar-collapse navbar-right">
                        <ul class="nav navbar-nav">
                            <li><a href="<?= base_url().'landing'?>">Home</a></li>
                            <li><a href="<?= base_url().'produk'?>">Produk</a></li>
                            <li><a href="<?= base_url().'artikel'?>">Artikel</a></li>                        
                           <li class="dropdown">
                              <a href="#">About </a>
                              <ul class="dropdown-menu">
                                <li><a href="components.html" class="bold">Profil </a></li>
                                <li><a href="icons.html" class="bold">Visi dan Misi</a></li>
                                <li><a href="animations.html" class="bold">Nilai Perusahaan</a></li>
                                <li><a href="icons.html" class="bold">Pencapaian dan Penghargaan</a></li>

                              </ul>
                            </li>
                            <li><a href="<?= base_url().'karir'?>">Karir</a></li>
                            <li><a href="portfolio.html">Log In</a></li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
               </div>
            </div>
        </nav>
    </header>
    <!--/.nav-ends -->
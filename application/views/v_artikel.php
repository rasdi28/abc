<section id="top_banner">
        <div class="banner">
            <div class="inner text-center">
            
            </div>
        </div>
        <div class="page_info">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-6">
                        <h4></h4>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6" style="text-align:right;">Home<span class="sep"> / </span><span class="current">Artikel</span></div>
                </div>
            </div>
        </div>

        </div>
    </section>

  <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="section-heading text-center">
                    <div class="col-md-12 col-xs-12">
                        <h1>Our <span>Artikel</span></h1>
                        <p class="subheading">Lorem ipsum dolor sit amet sit legimus copiosae instructior ei ut vix denique fierentis ea saperet inimicu ut qui dolor oratio mnesarchum ea utamur impetus fuisset nam nostrud euismod volumus ne mei.</p>
                    </div>
                </div>
            </div>
			<?php
				foreach ($data->result_array() as $j) :
						$post_id=$j['tulisan_id'];
						$post_judul=$j['tulisan_judul'];
						$post_isi=$j['tulisan_isi'];
						$post_author=$j['tulisan_author'];
						$post_image=$j['tulisan_gambar'];
						$post_tglpost=$j['tanggal'];
						$post_slug=$j['tulisan_slug'];
				?>            

            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 portfolio-item">
                    <div class="portfolio-one">
                        <div class="portfolio-head">
                            <div class="portfolio-img"><img src="<?php echo base_url().'assets/images/'.$post_image;?>"></div>
                            <div class="portfolio-hover">
                                <a class="portfolio-link" href="#"><i class="fa fa-link"></i></a>
                                
                            </div>
                        </div>
                        <!-- End portfolio-head -->
                        <div class="portfolio-content">
                            <h5 class="title"><a href="<?php echo base_url().'artikel/'.$post_slug;?>"><?php echo $post_judul;?></a></h5>
                            <span><?php echo $post_tglpost.' | '.$post_author;?></span>
                            <p><em><?php echo limit_words($post_isi,10).'...';?></em></p>
                            <p><a href="<?php echo base_url().'artikel/'.$post_slug;?>" class="btn btn-primary with-arrow">Selengkapnya <i class="icon-arrow-right"></i></a></p>
                        </div>
                        <!-- End portfolio-content -->
                    </div>


                </div>
			 <?php endforeach;?>
                <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination pagination-lg justify-content-end">
                            <li class="page-item">
                                <a class="page-link" href="#" tabindex="-1">Previous</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#">Next</a>
                            </li>
                        </ul>
                    </nav>

                </div>
            </div>

           </div>
         

</section>


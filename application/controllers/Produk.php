<?php 
class Produk extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_tulisan');
		$this->load->model('m_pengunjung');
        $this->m_pengunjung->count_visitor();
	}
	function index(){
		$x['post']=$this->m_tulisan->get_post_home();
		$this->load->view('template/header',$x);
		$this->load->view('v_produk',$x);
		$this->load->view('template/footer');
	}

	function list_minuman()
	{
		$this->load->view('template/header');
		$this->load->view('list_minuman');
		$this->load->view('template/footer');
	}

	
}
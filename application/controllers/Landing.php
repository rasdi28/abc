<?php 
class Landing extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model('m_tulisan');
		$this->load->model('m_pengunjung');
        $this->m_pengunjung->count_visitor();
	}
	function index(){
		$x['post']=$this->m_tulisan->get_post_home();
		$this->load->view('template/header',$x);
		$this->load->view('v_landing',$x);
		$this->load->view('template/footer');
	}
}